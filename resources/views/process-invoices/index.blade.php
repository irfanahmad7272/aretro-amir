@extends('layouts.admin.layout')

@section('content')
    <style>
        td {
            padding: 5px 2px;
        }
    </style>
    <!-- HEADER -->
    <div class="header">
        <div class="container-fluid">
            <!-- Body -->
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <!-- Title -->
                        <h1 class="header-title">
                            Generate Supplier Invoices
                        </h1>
                    </div>
                    <div class="col-auto">

                        <!-- Button -->
                        {{-- <a href="#!" class="btn btn-primary lift">
                    Create Report
                    </a> --}}

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .header-body -->

        </div>
    </div> <!-- / .header -->

    <!-- CARDS -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="col-12 div_alert">
                    @include('layouts.alerts')
                </div>
                <!-- Value  -->
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0 mb-3">
                            <div class="col-md-12">
                                <p class="text-warning">Note: CSV file format should be exact same that is used inside
                                    formula</p>
                            </div>
                        </div> <!-- / .row -->
                        <form action="{{ route('import-supplier-invoices') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row align-items-center">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="mb-3">
                                            <label for="formFile" class="form-label">Currency Rate [JPY to SEK]:</label>
                                            <div class="form-control-wrap">
                                                <input type="text" value="{{ $jpy_to_sek }}" class="form-control"
                                                    name="currency_rate" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Select Supplier</label>
                                        <div class="form-control-wrap">
                                            <select class="form-select mb-3" id="supplier" data-choices name="supplier">
                                                <option value="eco">ECO</option>
                                                <option value="auc">AUC</option>
                                                <option value="star">STAR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3" id="shipping-cost-div">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Shipping Cost <span
                                                id="currency-type">(SEK)</span></label>
                                        <div class="form-control-wrap">
                                            <input type="number" id="shipping-cost" class="form-control" name="shipping"
                                                value="300" id="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 d-none" id="commission-div">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Commission (%)</label>
                                        <div class="form-control-wrap">
                                            <input type="number" id="commission-percent" class="form-control"
                                                name="commission" value="" id="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Customs Fees (%)</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" value="3"
                                                name="custom_fees_percent" id="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 d-none">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Translate</label>
                                        <div class="form-control-wrap">
                                            <select class="form-select mb-3" data-choices name="translate">
                                                <option value="no" selected>No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- / .row -->
                            <div class="row align-items-center mt-5">
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Select Supplier Invoice File</label>
                                        <div class="form-control-wrap">
                                            <input type="file" class="form-control" name="file" required
                                                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3 mt-4">
                                        <button type="submit" class="btn btn-primary">Generate Invoice CSV</button>
                                    </div>
                                </div>
                            </div> <!-- / .row -->
                        </form>

                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#supplier").change(function() {


                if ($(this).val() == "eco") {
                    $("#currency-type").text("(SEK)")
                    $("#shipping-cost-div").removeClass("d-none");
                    $("#shipping-cost").val(300);
                    $("#commission-div").addClass("d-none");
                    $("#commission-percent").val("");
                } else if ($(this).val() == "auc") {
                    $("#currency-type").text("(JPY)")
                    $("#shipping-cost-div").removeClass("d-none");
                    $("#shipping-cost").val(2840);
                    $("#commission-percent").val(8);

                    $("#commission-div").removeClass("d-none");
                } else {
                    $("#shipping-cost-div").addClass("d-none");
                    $("#shipping-cost").val("");
                    $("#commission-div").addClass("d-none");
                    $("#commission-percent").val("");
                }
            })
        });
    </script>
@endsection
