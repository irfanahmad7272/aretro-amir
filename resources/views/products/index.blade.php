@extends('layouts.admin.layout')

@section('content')
    <style>
        td {
            padding: 5px 2px;
        }
    </style>
    <!-- HEADER -->
    <div class="header">
        <div class="container-fluid">
            <!-- Body -->
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <!-- Title -->
                        <h1 class="header-title">
                            Products
                        </h1>
                    </div>
                    <div class="col-auto">

                        <!-- Button -->
                        {{-- <a href="#!" class="btn btn-primary lift">
                    Create Report
                    </a> --}}

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .header-body -->

        </div>
    </div> <!-- / .header -->

    <!-- CARDS -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="col-12 div_alert">
                    @include('layouts.alerts')
                </div>
                <!-- Value  -->
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0 mb-3">
                            <div class="col-md-12">
                                <h2>Filter Products</h2>
                            </div>
                        </div> <!-- / .row -->
                        <form action="{{ route('products.index') }}" method="get">

                            <div class="row align-items-center">
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Search Product</label>
                                        <input type="text" value="{{ $keyword }}" name="keyword" id="product_sku"
                                            class="form-control" placeholder="Search...">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Stock Value</label>
                                        <div class="form-control-wrap">
                                            <select class="form-select mb-3" data-choices name="stock">
                                                <option value="all" {{ $stock == 'all' ? 'selected' : '' }}>All</option>
                                                <option value="instock" {{ $stock == 'instock' ? 'selected' : '' }}>Instock
                                                </option>
                                                <option value="outstock" {{ $stock == 'outstock' ? 'selected' : '' }}>Out of
                                                    stock</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="padding-left: 60px">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Total Instock</label>
                                        <div class="form-control-wrap">
                                            {{ $totalInstock }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="padding-left: 40px">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Total Outstock</label>
                                        <div class="form-control-wrap">
                                            {{ $totalOutstock }}

                                        </div>
                                    </div>
                                </div>

                            </div> <!-- / .row -->

                            <div class="row align-items-center gx-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary lift mt-3">Search</button>
                                    <a href="{{ route('products.index') }}" class="btn btn-danger mt-3">Reset</a>
                                </div>
                            </div> <!-- / .row -->
                        </form>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-12">

                <!-- Value  -->
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0 mb-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <?php $i = 1; ?>
                                    <table class="table table-striped" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Product ID</th>
                                                <th>SKU</th>
                                                <th>Product Name</th>
                                                <th>Category</th>
                                                <th>Brand</th>
                                                <th>Stock</th>
                                                <th>Price (Kr)</th>
                                                <th>Price for VC (Eur)</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $product->product_id }}</td>
                                                    <td>{{ $product->original_sku }}</td>
                                                    <td>{{ $product->product_name }}</td>
                                                    <td>{{ $product->category }}</td>
                                                    <td>{{ $product->brand }}</td>
                                                    <td>{{ $product->stock }}</td>
                                                    <td>{{ $product->selling_price_eur }}</td>
                                                    <td>{{ $product->vc_price_in_euros }}</td>
                                                    <td>
                                                        <form action="{{ route('products.delete') }}" method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                            <input type="hidden" name="product_id"
                                                                value="{{ $product->id }}" id="">
                                                            <button class="btn btn-danger btn-sm">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $products->links() }}
                                </div>
                            </div>
                        </div> <!-- / .row -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
