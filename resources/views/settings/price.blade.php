@extends('layouts.admin.layout')

@section('content')


<!-- HEADER -->
<div class="header">
    <div class="container-fluid">
        <!-- Body -->
        <div class="header-body">
            <div class="row align-items-end">
                <div class="col">
                    <!-- Title -->
                    <h1 class="header-title">
                        Commission Adjustments (%)
                    </h1>
                </div>
                <div class="col-auto">
  
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

<!-- CARDS -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="col-12 div_alert">
                @include('layouts.alerts')
            </div>
            <span class="d-none count-value">{{count($markets)}}</span>
            <?php  $b = []; ?>
            @foreach ($markets as $market)  
            <div class="card" id="marketp{{$loop->iteration}}">
                <div class="card-body">
                    <h3 class="card-title mb-4">{{$market->market_place}}</h3>
                    <div class="row align-items-center gx-0">
                        <div class="col-md-6">
                            <form id="price-form-id{{$loop->iteration}}" class="price-form-class">
                                <div class="form-check form-switch">
                                    <input type="hidden" name="market_id" value="{{$market->id}}" id="">
                                    <input class="form-check-input price-status-checkbox" type="checkbox" name="price_status" {{$market->status == 1 ? 'checked' : ""}} id="flexSwitchCheckDefault">
                                    <label class="form-check-label">Enable/Disable Percentage</label>
                                </div>
                                <div class="mb-4"></div>
                                <input type="tel" name="price_percentage" class="form-control price-percentage" id="price-percentage{{$loop->iteration}}">
                                {{-- <select class="form-select price-percentage" name="price_percentage" id="price-percentage{{$loop->iteration}}" aria-label="Default select example">
                                    <option value="10">10%</option>
                                    <option value="20">20%</option>
                                    <option value="30">30%</option>
                                </select> --}}
                            </form>
                            <?php $b[] = $market->percentage; ?>
                            
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            @endforeach

         <h2 class="perVal d-none">{{implode(',', $b)}}</h2>   


        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
    $( document ).ready(function() {

        $("#marketp1").addClass('d-none')
        let perVal = $(".perVal").text();

        $.each(perVal.split(','), function( index, value ) {
            var myIndex = index + 1;
            $("#price-percentage"+myIndex).val(value);
        });

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $('.price-status-checkbox').change(function() {
            let formId = $(this).parent().parent().attr('id');
            $.ajax({
                    type: "POST",
                    url: "{{route('price.update')}}",
                    data: $('#'+formId).serialize(),
                    success: function( msg ) {
                        if(msg.status == 1) {
                            toastr.success('Price Percentage for '+msg.market_place+' enabled');

                        } else {
                            toastr.error('Price Percentage for '+msg.market_place+' disabled');

                        }


                    },

                });
        });

        $('.price-percentage').change(function() {
            let formId = $(this).parent().attr('id');
            $.ajax({
                    type: "POST",
                    url: "{{route('price.update')}}",
                    data: $('#'+formId).serialize(),
                    success: function( msg ) {
                        toastr.success('Price Percentage for '+msg.market_place+' updated!');
                    },

                });
        });
    });
    </script>
@endsection
