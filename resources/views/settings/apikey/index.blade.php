@extends('layouts.admin.layout')

@section('content')


<!-- HEADER -->
<div class="header">
    <div class="container-fluid">
        <!-- Body -->
        <div class="header-body">
            <div class="row align-items-end">
                <div class="col">
                    <!-- Title -->
                    <h1 class="header-title">
                        API KEY Settings
                    </h1>
                </div>
                <div class="col-auto">
  
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

<!-- CARDS -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">

            <div class="col-12 div_alert">
                @include('layouts.alerts')
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center gx-0">
                        <div class="col-md-6">
                            <form action="{{route('apikey.store')}}" method="POST">
                                @csrf
                                <div class="mb-3">
                                    <label for="formFile" class="form-label">API Key</label>
                                    <input type="text" name="api_key" id="api_key" class="form-control" value="{{$api_key ? $api_key->api_key : ""}}" readonly>
                                </div>
                                <button type="submit" class="btn btn-primary lift mt-3">Generate API Key</button>
                            </form>
                        </div>

                    </div> <!-- / .row -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
$("#api_key").click(function () {
   $(this).select();
});
    </script>
@endsection
