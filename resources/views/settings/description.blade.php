@extends('layouts.admin.layout')

@section('content')


<!-- HEADER -->
<div class="header">
    <div class="container-fluid">
        <!-- Body -->
        <div class="header-body">
            <div class="row align-items-end">
                <div class="col">
                    <!-- Title -->
                    <h1 class="header-title">
                        Description Field Settings
                    </h1>
                </div>
                <div class="col-auto">
  
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

<!-- CARDS -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">

            <div class="col-12 div_alert">
                @include('layouts.alerts')
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center gx-0">
                        <div class="col-md-12">
                            <form action="{{route('description.general.update')}}" method="POST">
                                @csrf
                                <div class="mb-3">
                                    <p style="font-size: 18px"> <b>General Description Settings</b></p>
                                    <div class="form-check form-switch" id="gen_id">
                                        <input type="hidden" name="desc_id" id="desc_id" value="{{$general_desc->id}}" >
                                        <input class="form-check-input desc-checkbox" type="checkbox" name="gendesc_status" id="gendesc_status" {{$general_desc->status == 1 ? 'checked' : ""}} id="flexSwitchCheckDefault">
                                        <label class="form-check-label">Enable/Disable Settings</label>
                                    </div>
                                    <div class="mb-4"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="mb-3">Site Intro:</label>
                                            <textarea name="site_intro" id="siteIntro">{{$general_desc->site_intro}}</textarea>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <label class="mb-3">Prompt Text:</label>
                                            <textarea name="" id="promptText">{{$general_desc->prompt_text}}</textarea>
                                        </div> --}}
                                    </div>

                                    <div class="mb-4"></div>

                                </div>
                                <button type="submit" class="btn btn-primary lift mt-3">Save</button>
                            </form>
                        </div>

                    </div> <!-- / .row -->
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center gx-0">
                        <div class="col-md-12">
                            <form action="{{route('description.ahlens.update')}}" method="POST">
                                @csrf
                                <div class="mb-3">
                                    <p style="font-size: 18px"> <b>Ahlens Description Settings</b></p>
                                    <div class="form-check form-switch">
                                        <input type="hidden" name="ahlens_desc_id" value="{{$ahlens_desc->id}}" id="ahlens_desc_id">
                                        <input class="form-check-input desc-checkbox" type="checkbox" name="gendesc_status" {{$ahlens_desc->status == 1 ? 'checked' : ""}} id="flexSwitchCheckDefault">
                                        <label class="form-check-label">Enable/Disable Settings</label>
                                    </div>
                                    <div class="mb-4"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="mb-3">Site Intro:</label>
                                            <textarea name="site_intro" id="ahlens-siteIntro">{{$ahlens_desc->site_intro}}</textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="mb-3">Prompt Text:</label>
                                            <textarea name="prompt_text" id="ahlens-promptText">{{$ahlens_desc->prompt_text}}</textarea>
                                        </div>
                                    </div>

                                    <div class="mb-4"></div>

                                </div>
                                <button type="submit" class="btn btn-primary lift mt-3">Save</button>
                            </form>
                        </div>

                    </div> <!-- / .row -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script src="https://cdn.tiny.cloud/1/ik2dijdsq8hwr2psz9tgzsu1eqc2rt3bakqf37w2479gkupy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>        
<script>
    tinymce.init({
        selector: '#siteIntro',
        height : "320",
        plugins: [
            'advlist autolink link lists'
            ],
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
          'link bullist numlist | ',
    });

    tinymce.init({
        selector: '#ahlens-promptText',
        height : "320",
        plugins: [
            'advlist autolink link lists'
            ],
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
          'link bullist numlist | ',
    });

    tinymce.init({
        selector: '#ahlens-siteIntro',
        height : "320",
        plugins: [
            'advlist autolink link lists'
            ],
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
          'link bullist numlist | ',
    });

    tinymce.init({
        selector: '#promptText',
        height : "320",
        plugins: [
            'advlist autolink link lists'
            ],
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
          'link bullist numlist | ',
    });
</script>

<script>
    $( document ).ready(function() {

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $('.desc-checkbox').change(function() {
            let id = $(this).parent().attr('id');
            
            let values;
            if(id == "gen_id") {
                values = {
                'desc_id': $('#desc_id').val(),
                'desc_status': $(this).is(':checked')
                }
            } else {
                values = {
                'desc_id': $('#ahlens_desc_id').val(),
                'desc_status': $(this).is(':checked')
                }
            }

            $.ajax({
                    type: "POST",
                    url: "{{route('description.status')}}",
                    data: values,
                    success: function( msg ) {

                        if(msg.status == 1) {
                            toastr.success('Description for '+msg.market_place+' enabled');

                        } else {
                            toastr.error('Description for '+msg.market_place+' disabled');

                        }

                    },

                });
        });

        $('.price-percentage').change(function() {
            let formId = $(this).parent().attr('id');
            $.ajax({
                    type: "POST",
                    url: "{{route('price.update')}}",
                    data: $('#'+formId).serialize(),
                    success: function( msg ) {
                        toastr.success('Price Percentage for '+msg.market_place+' updated!');
                    },

                });
        });
    });
    </script>
@endsection
