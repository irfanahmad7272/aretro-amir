@extends('layouts.admin.layout')

@section('content')

<!-- HEADER -->
<div class="header">
    <div class="container-fluid">
        <!-- Body -->
        <div class="header-body">
            <div class="row align-items-end">
                <div class="col">
                    <!-- Title -->
                    <h1 class="header-title">
                        Ahlens
                    </h1>
                </div>
                <div class="col-auto">

                    <!-- Button -->
                    {{-- <a href="#!" class="btn btn-primary lift">
                    Create Report
                    </a> --}}

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

<!-- CARDS -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">

            <!-- Value  -->
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center gx-0 mb-3">
                        <div class="col-md-6">
                            <h4 class="mb-4">Ahlens CSV</h4>
                            @if ($ahlens)
                            <label for="">Click to download: </label>
                            <a href="{{$ahlens->file_url}}">{{$ahlens->file_url}}</a>
                            @else
                            <h2 class="text-danger mt-4">No Record Found</h2>
                            @endif

                        </div>

                    </div> <!-- / .row -->
                </div>
            </div>

            {{-- <div class="card">
                <div class="card-body">
                    <div class="row align-items-center gx-0 mb-3">

                        <div class="col-md-6">
                            <h4 class="mb-4">Ahlens Offers CSV</h4>
                            @if ($ahlens)
                            <label for="">Click to download: </label>
                            <a href="{{$ahlensOffers->file_url}}">{{$ahlensOffers->file_url}}</a>
                            @else
                            <h2 class="text-danger mt-4">No Record Found</h2>
                            @endif

                        </div>
                    </div> <!-- / .row -->
                </div>
            </div> --}}

        </div>
    </div>
</div>

@endsection