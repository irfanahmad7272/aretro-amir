<!DOCTYPE html>
<html lang="en">
  

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('admin/favicon/favicon.ico')}}" type="image/x-icon"/>
    
    
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/libs.bundle.css')}}" />
    
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/theme.bundle.css')}}" id="stylesheetLight" />
    <link rel="stylesheet" href="{{asset('admin/css/theme-dark.bundle.css')}}" id="stylesheetDark" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>body { display: none; }</style>
    
    <!-- Title -->
    <title>Dashkit</title>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156446909-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-156446909-1");</script>
  </head>


  <body>
    <div id="app">

      @include('layouts.admin.sidebar')

        <main class="main-content">

          @include('layouts.admin.topnav')
          
          @yield('content')

        </main>

    </div>
  </body>

      <!-- Vendor JS -->
      <script src="{{asset('admin/js/vendor.bundle.js')}}"></script>
    
      <!-- Theme JS -->
      <script src="{{asset('admin/js/theme.bundle.js')}}"></script>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      @yield('scripts')

</html>
