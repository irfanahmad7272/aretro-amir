<nav class="navbar navbar-vertical fixed-start navbar-expand-md navbar-light" id="sidebar">
    <div class="container-fluid">
  
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
  
      <!-- Brand -->
      <a class="navbar-brand" href="./index.html">
        <img src="{{asset('./admin/img/logo.svg')}}" class="navbar-brand-img mx-auto" alt="...">
      </a>
  

  
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidebarCollapse">
  
        <hr class="navbar-divider my-3">

        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'home') ? 'active' : ''}}"   href="{{route('home')}}" role="button" aria-expanded="true" aria-controls="sidebarDashboards">
                <i class="fe fe-grid"></i> Dashboard
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'products') ? 'active' : ''}}" href="{{(route('products.index'))}}">
              <i class="fe fe-box"></i> Products
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'process-supplier-invoices') ? 'active' : ''}}" href="{{(route('process-supplier-invoices'))}}">
              <i class="fe fe-file-text"></i> Generate Supplier Invoices
            </a>
          </li>
 
        </ul>

        <!-- Divider -->
        <hr class="navbar-divider my-3">
        <h6 class="navbar-heading">
          Market Places
        </h6>

        <!-- Navigation -->
        <ul class="navbar-nav">
          {{-- <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'vintage-bar') ? 'active' : ''}}" href="{{(route('vintage_bar'))}}">
              <span class="fe fe-stop-circle"></span> Vintagebar
            </a>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'vestiaire') ? 'active' : ''}}" href="{{(route('vestiaire'))}}">
              <i class="fe fe-stop-circle"></i> Vestiaire
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'ahlens') ? 'active' : ''}}" href="{{(route('ahlens'))}}">
              <i class="fe fe-stop-circle"></i> Ahlens
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'rebelle') ? 'active' : ''}}" href="{{(route('rebelle'))}}">
              <i class="fe fe-stop-circle"></i> Rebelle
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'mpd') ? 'active' : ''}}" href="{{(route('mpd'))}}">
              <i class="fe fe-stop-circle"></i> MPD
            </a>
          </li>
  
        </ul>

        <!-- Divider -->
        <hr class="navbar-divider my-3">
        <h6 class="navbar-heading">
          Settings
        </h6>
        <ul class="navbar-nav">
                
          {{-- <li class="nav-item">
              <a class="nav-link {{(request()->segment(1) == 'apikey-settings') ? 'active' : ''}}" href="{{route('apikey.index')}}">
                  <i class="fe fe-settings"></i> API Key Settings
              </a>
          </li>  --}}
          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'price-adjustments') ? 'active' : ''}}" href="{{route('price.index')}}">
                <i class="fe fe-settings"></i> Commission Adjustments
            </a>
          </li> 

          <li class="nav-item">
            <a class="nav-link {{(request()->segment(1) == 'description-settings') ? 'active' : ''}}" href="{{route('description.index')}}">
                <i class="fe fe-settings"></i> Description Field Settings
            </a>
          </li> 

          

        </ul>
        
        <ul class="navbar-nav mt-5">
            <li class="nav-item">
                <a class="ms-4 btn btn-outline-danger w-75" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="fe fe-log-out"></i> Logout
                </a>
                <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                    @csrf                  
                </form>
            </li>
        </ul>


        <!-- Push content down -->
        <div class="mt-auto"></div>
  

  
      </div> <!-- / .navbar-collapse -->
  
    </div>
  </nav>