@if ($errors->any())
<div class="alert alert-danger alert-dismissible mb-4" role="alert" style="background-color: #f8d7da; border-color:#f5c6cb; color:#721c24; margin-bottom: 0px;">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert" >
    {{session()->get('success')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div> 
@endif

@if (session('warning'))
<div class="alert alert-warning alert-dismissible" role="alert">
    {{ session('warning') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session('danger'))
<div class="alert alert-danger alert-dismissible" role="alert" >
    {{session()->get('danger')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div> 
@endif