<?php

namespace App\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;



class CurrencyController extends Controller
{
    public $convertFrom;
    public $convertTo;
    public function __construct($convertFrom, $convertTo)
    {
        $this->convertFrom = $convertFrom;
        $this->convertTo = $convertTo;
    }
    public function currency()
    {

        $exchangeRates = new ExchangeRate();
        $result = $exchangeRates->exchangeRate($this->convertFrom, $this->convertTo);
        return $result;
    }

}
