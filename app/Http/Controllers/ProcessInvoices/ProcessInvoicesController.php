<?php

namespace App\Http\Controllers\ProcessInvoices;

use App\Exports\AUCExport;
use Illuminate\Http\Request;
use App\Exports\EcoInvoiceExport;
use App\Exports\StarExport;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;


class ProcessInvoicesController extends Controller
{
    public function index()
    {
        return view('process-invoices.index', [
            'jpy_to_sek' => Setting::first('jpy_to_sek')->jpy_to_sek
        ]);
    }

    public function import_supplier_invoices(Request $request)
    {

        // Load data from the input Excel file
        $data = Excel::toCollection(new class implements FromCollection
        {
            public function collection()
            {
                return collect();
            }
        }, $request->file('file'))[0];

        if ($request->supplier == "eco") {
            return Excel::download(new EcoInvoiceExport($data, $request->except(['file', '_token'])), "{$request->supplier}_invoice.csv");
        }
        if ($request->supplier == "auc") {
            return Excel::download(new AUCExport($data, $request->except(['file', '_token'])), "{$request->supplier}_invoice.csv");
        }
        if ($request->supplier == "star") {
            return Excel::download(new StarExport($data, $request->except(['file', '_token'])), "{$request->supplier}_invoice.csv");
        }
    }
}
