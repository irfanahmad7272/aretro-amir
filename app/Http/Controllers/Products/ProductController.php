<?php

namespace App\Http\Controllers\Products;


use App\Exports\MPDExport;
use App\Models\MarketPlace;
use App\Models\SaveProduct;
use App\Exports\OfferExport;
use Illuminate\Http\Request;
use App\Exports\AhlensExport;
use App\Exports\RebelleExport;
use App\API\CurrencyController;
use App\Exports\VestiaireExport;
use App\Exports\VintageBarExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Codexshaper\WooCommerce\Facades\Product;


class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = SaveProduct::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->where('original_sku', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_name', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_id', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('description', 'Like', '%' . $request->keyword . '%');
            });
        })->when($request->stock && $request->stock !== 'all', function ($q) use ($request) {
            if ($request->stock === 'instock') {
                $q->where('stock', '>', 0);
            }
            if ($request->stock === 'outstock') {
                $q->where('stock', '<=', 0);
            }
        })
            ->orderBy('stock', 'desc')
            ->paginate(50)
            ->withQueryString();


        $keyword = $request->keyword;
        $stock = $request->stock;
        $totalInstock = SaveProduct::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->where('original_sku', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_name', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_id', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('description', 'Like', '%' . $request->keyword . '%');
            });
        })->where('stock', '>', 0)->count();
        $totalOutstock = SaveProduct::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->where('product_name', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_name', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('product_id', 'Like', '%' . $request->keyword . '%')
                    ->orWhere('description', 'Like', '%' . $request->keyword . '%');
            });
        })->where('stock', '<=', 0)->count();
        return view('products.index', compact('products', 'keyword', 'stock', 'totalInstock', 'totalOutstock'));
    }

    public function delete(Request $request)
    {
        $product = SaveProduct::find($request->product_id);

        $product->delete();

        return redirect()->back()->with('success', 'Product deleted successfully!');
    }
    public function importProducts(Request $request)
    {
        return Excel::download(new VintageBarExport(), 'vintagebar.csv');
    }

    public function vintage_bar()
    {
        $vintagebar = MarketPlace::where('market_place', 'Vintagebar')->first();
        return view('vintagebar.index', compact('vintagebar'));
    }

    public function vintagebar_export()
    {
        return Excel::download(new VintageBarExport, 'vintagebar.csv');
    }

    public function vestiaire()
    {
        $vestiaire = MarketPlace::where('market_place', 'Vestiaire')->first();
        return view('vestiaire.index', compact('vestiaire'));
    }

    public function mpd()
    {
        $mpd = MarketPlace::where('market_place', 'Mpd')->first();
        return view('mpd.index', compact('mpd'));
    }

    public function vestiaire_export()
    {
        return Excel::download(new VestiaireExport, 'vestiaire.csv');
    }

    public function ahlens()
    {
        $ahlens = MarketPlace::where('market_place', 'Ahlens')->first();
        $ahlensOffers = MarketPlace::where('market_place', 'Ahlens Products')->first();
        return view('ahlens.index', compact('ahlens', 'ahlensOffers'));
    }


    public function ahlens_export()
    {
        return Excel::download(new AhlensExport, 'ahlensproducts.xlsx');
    }

    public function ahlensoffers_export()
    {
        return Excel::download(new OfferExport, 'ahlensoffers.xlsx');
    }

    public function rebelle()
    {
        $rebelle = MarketPlace::where('market_place', 'Rebelle')->first();
        return view('rebelle.index', compact('rebelle'));
    }

    public function rebelle_export()
    {
        return Excel::download(new RebelleExport, 'rebelle.csv');
    }

    public function mpd_export()
    {
        return Excel::download(new MPDExport, 'mpd.csv');
    }

    public function job_test()
    {
        $getEuro = new CurrencyController('SEK', 'CHF');

        $options = [
            'per_page' => 100,
            'page' => 1,
            'after' => '2021-01-01T00:00:00Z',
            'lang' => 'en',
            'status' => 'publish'
        ];

        $products = Product::options(['per_page' => 10, 'page' => 30 , 'after' => '2021-01-01T00:00:00Z', 'lang' => 'en'])->get();


        foreach ($products as $product) {
            //
        }

    }
}
