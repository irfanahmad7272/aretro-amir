<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\ApiKey;
use App\Models\SaveProduct;
use Illuminate\Http\Request;
use Codexshaper\WooCommerce\Facades\Product;

class APIKeyController extends Controller
{
    public function index()
    {
        $api_key = ApiKey::first();
        return view('settings.apikey.index', compact('api_key'));
    }

    public function store(Request $request)
    {
        if($request->api_key == "") {
            $api_key = new ApiKey;
        } else {
            $api_key = ApiKey::first();
        }

        $api_key->api_key = bcrypt('aretro_api_key');
        $api_key->save();
        
        return redirect()->back()->with('success', 'API Key updated successfully!');
    }


    public function price_test()
    {
       return $products = Product::options(['per_page' => 20, 'page' => 1, 'after' => '2021-01-01T00:00:00Z', 'lang' => 'en' ])->get('regular_price');

    }

    public function truncate_prodcts_tbl()
    {
        SaveProduct::truncate();
        return "All products deleted";
    }
}
