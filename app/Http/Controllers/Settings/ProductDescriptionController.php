<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\DescriptionSettings;
use Illuminate\Http\Request;

class ProductDescriptionController extends Controller
{
    public function index()
    {
        $general_desc = DescriptionSettings::where('market_place', 'General')->first();
        
        $ahlens_desc = DescriptionSettings::where('market_place', 'Ahlens')->first();
        
        return view('settings.description', compact('general_desc', 'ahlens_desc'));
    }

    public function descriptionstatus(Request $request)
    {
        $desc = DescriptionSettings::find($request->desc_id);
        
        if($request->desc_status == "true") {
            $desc->status = 1;
        } else {
            $desc->status = 0;
        }

        $desc->save();

        return $desc;
    }


    public function update_general_desc(Request $request)
    {
        $desc = DescriptionSettings::where('market_place', 'General')->first();
        if($desc == "") {
            $gendesc = new DescriptionSettings(); 
        } else {
            $gendesc = DescriptionSettings::where('market_place', 'General')->first();
        }

        $gendesc->site_intro = $request->site_intro; 

        // $gendesc->prompt_text =   $request->prompt_text; 
        
        $gendesc->save();

        return redirect()->back()->with('success', 'General Description settings updated successfully!');
    }


    public function update_ahlens_desc(Request $request)
    {
        $desc = DescriptionSettings::where('market_place', 'Ahlens')->first();
        if($desc == "") {
            $ahlens_desc = new DescriptionSettings(); 
        } else {
            $ahlens_desc = DescriptionSettings::where('market_place', 'Ahlens')->first();
        }

        $ahlens_desc->site_intro = $request->site_intro; 

        $ahlens_desc->prompt_text =   $request->prompt_text; 
        
        $ahlens_desc->save();

        return redirect()->back()->with('success', 'Ahlens Description settings updated successfully!');

    }
}
