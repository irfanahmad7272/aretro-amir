<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\PriceAdjustment;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function index()
    {
        $vestiaire = PriceAdjustment::where('market_place', 'vestiaire')->first();

        $markets = PriceAdjustment::all();
        return view('settings.price', compact('vestiaire', 'markets'));
    }


    public function update(Request $request)
    {
        $market = PriceAdjustment::find($request->market_id);

        if($request->price_status) {
            $market->status = 1;
        } else {
            $market->status = 0;
        }

        $market->percentage = $request->price_percentage;

        $market->save();

        return $market;
    }
}
