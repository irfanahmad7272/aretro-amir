<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\Setting;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;

class FetchCurrencyRatesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $exchangeRates = app(ExchangeRate::class);

        $result = $exchangeRates->exchangeRate('SEK', ['EUR', 'CHF']);
        $resultJPY = $exchangeRates->exchangeRate('JPY', ['SEK']);

        $settings = Setting::first();
        $settings->currency_euro = $result["EUR"];
        $settings->currency_swiss_franc = $result["CHF"];
        $settings->jpy_to_sek = $resultJPY["SEK"];
        $settings->save();
    }
}
