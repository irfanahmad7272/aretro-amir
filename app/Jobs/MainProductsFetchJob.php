<?php

namespace App\Jobs;

use Throwable;
use Illuminate\Bus\Queueable;
use App\Jobs\ChildProductsFetchJob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Codexshaper\WooCommerce\Facades\Product;


class MainProductsFetchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $options = [
            'per_page' => 100,
            'page' => 1,
            'after' => '2021-01-01T00:00:00Z',
            'lang' => 'en',
            'status' => 'publish' 
            ];
    
        try {  
            $totalPages =  Product::paginate(100, 1, $options)['meta']['total_pages'];
            
        
            for($i = 1; $i <= $totalPages; $i++){
                
                $options['page'] = $i;
                ChildProductsFetchJob::dispatch($options)
                ->onQueue('fetchChildQueue');
            }
        }
        catch(Throwable $e){
            die("Can't get products: $e");
        }
    }
}
