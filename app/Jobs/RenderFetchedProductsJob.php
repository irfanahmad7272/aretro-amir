<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\SaveProduct;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RenderFetchedProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $products;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($products)
    {
        $this->products = $products;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $products = $this->products;
        foreach($products as $product) {

                $vc_cat = "";
                $swe_desc = "";
                $product_color_name = "";
                $product_material = "";
                $product_band_material = "";
                $product_movement = "";
                $product_vc_condition = "";
                $product_vc_material = "";
                $product_vc_model = "";
                $product_vc_vintage = "";
                $product_brand_value = "";
                $hide_this_product_at_vc = null;
                $hide_this_product_at_tvb = null;
                $hide_this_product_at_rebelle = null;

                if(!isset($product->vc_category) && empty($product->vc_category)) {

                    $vc_cat = "Women > Bags > Handbags > Bags";


                } else {
                    $vc_cat = $product->vc_category[0]->name;
                }


                if($product->regular_price == null || $product->regular_price == "") {
                    continue;
                }            


                $image_url = "";
                $image_urli = 0;
                foreach ($product->images  as $image) {
                    if($image_urli == 0) {
                        $image_url .= $image->src;

                    } else {
                        $image_url .= ", ". $image->src;
                    }
                    $image_urli ++;
                }

                $getCategories = "";
                $count = 0;
                foreach ($product->categories  as $category) {
                    if($count == 0) {
                        $getCategories .= $category->name;

                    } else {
                        $getCategories .= ", ". $category->name;
                    }
                    $count ++;
                }

                // $sku = str_replace(['-', '(', ')'], ' ', $product->sku);
                $sku = preg_replace('/[^A-Za-z0-9]/', ' ', $product->sku);
                $description = preg_replace('#<a.*?>.*?</a>#i', '', $product->description);

                foreach($product->meta_data as $meta) {
                    if($meta->key == "product_swedish_description") {
                         $swe_desc = strip_tags($meta->value); 
                    }

                    if($meta->key == "product_color_value") {
                        $product_color_name =  $meta->value; 
                    }

                    if($meta->key == "product_brand_value") {
                        $product_brand_value = $meta->value;
                    }

                    if($meta->key == "product_band_material") {

                        $product_band_material =  $meta->value; 
                    }

                    if($meta->key == "product_vc_mechanism") {
                        $product_movement =  $meta->value; 
                    }

                    if($meta->key == "product_vc_condition") {
                        $product_vc_condition =  $meta->value; 
                    }

                    if($meta->key == "product_material_value") {
                        $product_vc_material =  $meta->value; 
                    }

                    if($meta->key == "product_model_value") {
                        $product_vc_model =  $meta->value; 
                    }

                    if($meta->key == "product_vintage_value") {
                        $product_vc_vintage =  $meta->value; 
                    } 

                    
                    if($meta->key == "hide_this_product_at_vc") {
                        $hide_this_product_at_vc =  $meta->value; 
                    } 

                    if($meta->key == "hide_this_product_at_tvb") {
                        $hide_this_product_at_tvb =  $meta->value; 
                    } 

                    if($meta->key == "hide_this_product_at_rebelle") {
                        $hide_this_product_at_rebelle =  $meta->value; 
                    } 
                }
                
                if(SaveProduct::where('product_id', $product->id)->exists()) {
                    $saveProduct =  SaveProduct::where('product_id', $product->id)->first();
                } else {
                    $saveProduct = new SaveProduct;
                }
                

                $saveProduct->product_id = $product->id;
                $saveProduct->stock = $product->stock_quantity;
                $saveProduct->stock_status = $product->stock_status;
                $saveProduct->product_image_url = $image_url;
                $saveProduct->sku = $sku;
                $saveProduct->selling_price_eur = $product->regular_price;
                $saveProduct->brand = $product->gpf_data->brand;
                $saveProduct->brand_short =  "";
                $saveProduct->product_name = $product->name;
                $saveProduct->description = $description;
                $saveProduct->product_swedish_description = $swe_desc;
                $saveProduct->product_color_name = $product_color_name;
                $saveProduct->gender = $product->gpf_data->gender ? $product->gpf_data->gender : "Female" ;
                $saveProduct->main_category = "";
                $saveProduct->category = $getCategories;
                $saveProduct->category_short = "";
                $saveProduct->vc_category = htmlspecialchars_decode($vc_cat) ;
                $saveProduct->vc_product_condition =  $product_vc_condition ? $product_vc_condition : "Very good condition";
                $saveProduct->vc_product_material = $product_vc_material;
                $saveProduct->vc_product_model = $product_vc_model;
                $saveProduct->vc_product_vintage = $product_vc_vintage;
                $saveProduct->vc_brands = $product_brand_value;
                $saveProduct->product_material = $product_material;
                $saveProduct->product_band_material = $product_band_material;
                $saveProduct->product_movement = $product_movement;
                $saveProduct->type_of_material = "";
                $saveProduct->name_of_material = $product->gpf_data->material;
                $saveProduct->type_of_material2 = "";
                $saveProduct->name_of_material2 = "";
                $saveProduct->style = "";
                $saveProduct->gender = $product->gpf_data->gender;
                $saveProduct->color1 = $product_color_name;
                $saveProduct->shade1 = "";
                $saveProduct->color2 = "";
                $saveProduct->shade2 = "";
                $saveProduct->rrp = "";
                $saveProduct->length = $product->dimensions->length;
                $saveProduct->width = $product->dimensions->width;
                $saveProduct->depth = "";
                $saveProduct->shoulder_drop = "";
                $saveProduct->hand_drop = "";
                $saveProduct->diameter = "";
                $saveProduct->circumference = "";
                $saveProduct->ring_side = "";
                $saveProduct->belt_size = "";
                $saveProduct->caret = "";
                $saveProduct->dust_bag = "";
                $saveProduct->box = "";
                $saveProduct->authenticity_card = "";
                $saveProduct->comments = "";
                $saveProduct->rating = $product->average_rating;
                $saveProduct->country_of_origin = "";
                $saveProduct->directory = "";
                $saveProduct->photos = "";
                $saveProduct->hide_at_vc = $hide_this_product_at_vc;
                $saveProduct->hide_at_tvb = $hide_this_product_at_tvb;
                $saveProduct->hide_at_rebelle = $hide_this_product_at_rebelle;
                
                $saveProduct->save();
                // return false;

            }
    }
}
