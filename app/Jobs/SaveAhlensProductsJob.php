<?php

namespace App\Jobs;

use App\Exports\AhlensExport;
use App\Exports\OfferExport;
use App\Models\MarketPlace;
use Illuminate\Bus\Queueable;
use App\Exports\VestiaireExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SaveAhlensProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // ini_set('max_execution_time', 180);        

        Excel::store(new AhlensExport(), 'ahlens.csv', 'public');
        
        // Excel::store(new OfferExport(), 'vintagebar.csv', 'public');

        $marketplace = MarketPlace::where('market_place', 'Ahlens')->first();
        if($marketplace == "") {
            $marketplace = new MarketPlace(); 
        } else {
            $marketplace = MarketPlace::where('market_place', 'Ahlens')->first();
        }

        $marketplace->market_place = 'Ahlens'; 

        $marketplace->file_url =   asset("/storage")."/". 'ahlens.csv'; 
        
        $marketplace->save();

    }
}
