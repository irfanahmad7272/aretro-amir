<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebelleBrand extends Model
{
    use HasFactory;

    protected $guarded = [];
}
