<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\Setting;
use App\Models\SaveProduct;
use App\API\CurrencyController;
use App\Models\PriceAdjustment;
use App\Models\DescriptionSettings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;



class VestiaireExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison, WithCustomCsvSettings
{
    // use Exportable;

    // private $headers;

    // public function __construct()
    // {
    //     $this->headers = [
    //         'Content-Type' => 'text/csv' 
    //     ];
    // }

    public function getCsvSettings(): array
    {
        return [
            'use_bom' => false
        ];
    }

    public function headings():array{
        return[
            'SKU',
            'Universe',
            'Category',
            'Brand',
            'Subcategory',
            'Sub Sub Category',
            'Material',
            'Color',
            'Pattern',
            'Model',
            'Sizing',
            'Size',
            'Images',
            'Currency',
            'Price',
            'Description',
            'Vintage',
            'Condition',
            'Inventory',
            'Mechanism',
            'Band Material',
            'Dimension'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $item = collect();

        // $getEuro = new CurrencyController('SEK', 'EUR');

        $getEuro = Setting::first();

        $euro = $getEuro->currency_euro;

        $products = SaveProduct::all();


        $defaultProductImage = "https://aretrotale.com/wp-content/uploads/2021/10/product-default-img.png";

        foreach($products as $key => $product) {

            // if($product->created_at <= Carbon::now()->subDays(30) && $product->stock <= 0) { 
            //     continue;
            // }

            if($product->product_image_url == "") {
                $image = $defaultProductImage;
            } else {
                $image = str_replace(",",";",$product->product_image_url);
            }
            $vc_material =  explode('>',$product->vc_product_material);

            if(isset($vc_material[3])) {
                $material = $vc_material[3];
            } else {
                $material = "Leather";
            }

            $vc_model =  explode('>',$product->vc_product_model);

            if(isset($vc_model[4])) {
                $model = $vc_model[4];
            } else {
                $model = "";
            }

            $vc_band_material =  explode('>',$product->product_band_material);


            if(isset($vc_band_material[3])) {
                $band_material = trim($vc_band_material[3]);
            } else {
                $band_material = "Other";
            }



            
            $vc_cat = explode('>',$product->vc_category);

            if(!isset($vc_cat[0])) {
                $vc_cat[0] = "";
            }
            if(!isset($vc_cat[1])) {
                $vc_cat[1] = "";
            }
            if(!isset($vc_cat[2])) {
                $vc_cat[2] = "";
            }
            if(!isset($vc_cat[3])) {
                $vc_cat[3] = "";
            }

            $productsCollection = [];

            array_push($productsCollection, trim($product->sku).'-VC');
            array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[0]) : "Women"); //Universe

            
            if (strpos($product->product_name, 'Watch') !== false) {
                array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[1]) : "Accessories" ); //Caregory
                array_push($productsCollection, $product->stock_status == "instock" ? trim($product->vc_brands) : 'Rolex'); //Brand
                array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[2]) : 'Watches' ); //SubCaregory
                array_push($productsCollection, ""); //SubSubCaregory 

            } else {
                array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[1]) : "Bags" ); //Caregory
                array_push($productsCollection, $product->stock_status == "instock" ? trim($product->vc_brands) : 'Gucci'); //Brand
                array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[2]) : 'Handbags' ); //SubCaregory
                if(isset($vc_cat[3]) && !empty($vc_cat[3])) {
                    array_push($productsCollection, $product->stock_status == "instock" ? trim($vc_cat[3]) : 'Bags'); //SubSubCaregory 
                } else {
                    array_push($productsCollection, $product->stock_status == "instock" ? "" : "Bags"); //SubSubCaregory 
    
                }

            }







            if (strpos($product->product_name, 'Watch') !== false) {
                array_push($productsCollection, "Steel"); //material
            } else {
                array_push($productsCollection, trim($material)); //material
            }

            array_push($productsCollection, $product->stock_status == "instock" ? trim($product->product_color_name) : 'Black');
            array_push($productsCollection, ""); 
            array_push($productsCollection, trim($model)); //model
            array_push($productsCollection, ""); //size
            array_push($productsCollection, ""); //size
            array_push($productsCollection,  $image);
            array_push($productsCollection, "EUR"); //currency

            array_push($productsCollection, $product->vc_price_in_euros);

            // $general_desc = DescriptionSettings::where('market_place', 'General')->first();

            // if($general_desc->status == 1) {
            //     $newdesc = strip_tags($product->description) . PHP_EOL . PHP_EOL . strip_tags($general_desc->site_intro);
            // } else {
            //     $newdesc = strip_tags($product->description);
            // }
            
            array_push($productsCollection, strip_tags($product->description));
            array_push($productsCollection, $product->vc_product_vintage ? trim($product->vc_product_vintage) : null);
            array_push($productsCollection, trim($product->vc_product_condition));  /* Condition */
            if($product->hide_at_vc == 1) {
                array_push($productsCollection,  0);

            } else {
                if($product->stock_status == "instock") {
                    array_push($productsCollection, $product->stock > 0 ? 1 : 0);
                } else {
                    array_push($productsCollection, 0);
                }
            }
            array_push($productsCollection, $product->product_movement ? trim($product->product_movement) : null); //Mechanism

            
            if (strpos($product->product_name, 'Watch') !== false) {
                array_push($productsCollection, $band_material); //Band Material
            } else {
                array_push($productsCollection, ""); //Band Material
            }
            array_push($productsCollection, "");

            $item->push($productsCollection);


        }

        return $item;
    }
}
