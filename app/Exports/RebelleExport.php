<?php

namespace App\Exports;

use App\Models\Setting;
use App\Models\SaveProduct;
use App\API\CurrencyController;
use App\Models\PriceAdjustment;
use App\Models\RebelleMaterial;
use App\Models\DescriptionSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class RebelleExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    public function headings():array{
        return[
            'external_id',
            'brand_id',
            'subcategory_id',
            'material_id',
            'color_id',
            'condition_id',
            'season_id',
            'size_id',
            'model_id',
            'price',
            'description',
            'image_url_1',
            'image_url_2',
            'image_url_3',
            'image_url_4',
            'image_url_5',
            'image_url_6',
            'image_url_7',
            'image_url_8',
            'image_url_9',
            'image_url_10',
            'tax'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $materialsCollection = [
            // "348615" => "pro",
            "gold and steel" => 794,
            "Leather" => 803,
            'Steel' => 815,
            'Polyester' => 811,
            'Denim - Jeans' => 798,
            'Jeans' => 798,
            'Synthetic' => 816,
            'Patent leather' => 802,
            'Silk' => 813,
        ];

        $conditionCollection = [
            "Never worn" => 893,
            "Very good condition" => 894,
            "Good condition" => 895,
            "Used" => 896,
            "New" => 1617
        ];

        $colorCollection = [
            "other_colors" => 871,
            "beige" => 872,
            "blue" => 873,
            "brown" => 875,
            "yellow" => 876,
            "patterned" => 877,
            "gold" => 878,
            "grey" => 879,
            "green" => 880,
            "khaki" => 881,
            "orange" => 882,
            "pink" => 883,
            "red" => 884,
            "turquoise" => 885,
            "violet" => 886,
            "black" => 887,
            "silver" => 888,
            "white" => 889,
            "taupe" => 940,
            "nude" => 941,
            "bordeaux" => 942,
            "creme" => 962,
            "Petrol" => 1037,
            "Fuchsia" => 1079,
            "Oliv" => 1080,
            "Ocker" => 1081,
            "black_white" => 1212
        ];


        $item = collect();

        $getEuro = Setting::first();

        $euro = $getEuro->currency_euro;

        $products = SaveProduct::where('stock', '>', 0)
        ->where(function($q) {
            $q->whereNull('hide_at_rebelle')
            ->orWhere('hide_at_rebelle' , '!=', 1);
        })
        ->get();


        // $defaultProductImage = "https://aretrotale.com/wp-content/uploads/2021/10/product-default-img.png";
        $defaultProductImage = "";

        foreach($products as $key => $product) {

            $rebelle_cat_brand_model =  explode('>',$product->rebelle_sub_category_brand_model);
            $cat_id = isset($rebelle_cat_brand_model[0]) ? trim($rebelle_cat_brand_model[0]) : "";
            $brand_id = isset($rebelle_cat_brand_model[1]) ? trim($rebelle_cat_brand_model[1]) : "";
            $model_id = isset($rebelle_cat_brand_model[2]) ? trim($rebelle_cat_brand_model[2]) : "";
            $vc_material =  explode('>',$product->vc_product_material);

            $productsCollection = [];

            array_push($productsCollection, $product->original_sku);
            array_push($productsCollection, $brand_id);
            array_push($productsCollection, $product->rebelle_subcategories);

            if(isset($vc_material[3])) {
                if (strpos($product->product_name, 'Watch') !== false) {
                    array_push($productsCollection, $materialsCollection["Steel"]); //material 815
                } else {
                    if(array_key_exists(trim($vc_material[3]), $materialsCollection)) {
                        array_push($productsCollection, $materialsCollection[trim($vc_material[3])]); //material
                    } else {
                        array_push($productsCollection, 823); //material
                    }
                }
            } else {
                array_push($productsCollection, 823); // other material
            }

            if(array_key_exists(strtolower($product->product_color_name), $colorCollection)) {
                array_push($productsCollection, $colorCollection[strtolower($product->product_color_name)]);
            } else {
                array_push($productsCollection, 871);
            }

            $condition_id =  $conditionCollection[trim($product->vc_product_condition)];



            array_push($productsCollection, $condition_id);  /* Condition */

           array_push($productsCollection, 890); /* Season */
           array_push($productsCollection, "");
           array_push($productsCollection, $model_id);


            $price = $euro * floatval($product->selling_price_eur);
            $priceinEuro = $euro * floatval($product->selling_price_eur);

            $getPrice = PriceAdjustment::where('market_place', 'Rebelle')->first();

            if($getPrice->status == 1) {
                $percent = $priceinEuro *  floatval($getPrice->percentage) / 100;
                $price = $priceinEuro + $percent;
            } else {
                $price = $priceinEuro;
            }
            array_push($productsCollection, round( $price ));

            $general_desc = DescriptionSettings::where('market_place', 'General')->first();

            if($general_desc->status == 1) {
                $newdesc = strip_tags($product->description) . PHP_EOL . PHP_EOL . strip_tags($general_desc->site_intro);
            } else {
                $newdesc = strip_tags($product->description);
            }

            array_push($productsCollection, $newdesc);

            if($product->product_image_url == "") {
                if($product->rebelle_product_white_background_photo) {
                    array_push($productsCollection, $product->rebelle_product_white_background_photo);
                } else {
                    array_push($productsCollection, $defaultProductImage);
                }
                array_push($productsCollection, $defaultProductImage);
                array_push($productsCollection, $defaultProductImage);
                array_push($productsCollection, $defaultProductImage);
                array_push($productsCollection, $defaultProductImage);

            } else {
                $images = explode(',',$product->product_image_url);
                if($product->rebelle_product_white_background_photo) {
                    array_push($productsCollection, $product->rebelle_product_white_background_photo);
                } else {
                    if(isset($images[0])){
                        $set_image = trim($images[0]);
                        array_push($productsCollection, $images[0]);
                    } else {
                        $set_image = $defaultProductImage;
                        array_push($productsCollection, $defaultProductImage);
                    }
                }

                if(isset($images[1])){
                    $set_image = trim($images[1]);
                    array_push($productsCollection, $images[1]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }
                if(isset($images[2])){
                    $set_image = trim($images[2]);
                    array_push($productsCollection, $set_image);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }
                if(isset($images[3])){
                    $set_image = trim($images[3]);
                    array_push($productsCollection, $images[3]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[4])){
                    $set_image = trim($images[4]);
                    array_push($productsCollection, $images[4]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[5])){
                    $set_image = trim($images[5]);
                    array_push($productsCollection, $images[5]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[6])){
                    $set_image = trim($images[6]);
                    array_push($productsCollection, $images[6]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[7])){
                    $set_image = trim($images[7]);
                    array_push($productsCollection, $images[7]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[8])){
                    $set_image = trim($images[8]);
                    array_push($productsCollection, $images[8]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }

                if(isset($images[9])){
                    $set_image = trim($images[9]);
                    array_push($productsCollection, $images[9]);
                } else {
                    $set_image = $defaultProductImage;
                    array_push($productsCollection, $defaultProductImage);
                }


            }


            array_push($productsCollection, 0);


            $item->push($productsCollection);


        }

        return $item;
    }
}
