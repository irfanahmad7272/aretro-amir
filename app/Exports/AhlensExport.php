<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\SaveProduct;
use App\Models\PriceAdjustment;
use App\Models\DescriptionSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;




class AhlensExport implements FromCollection, WithHeadings, ShouldAutoSize, Responsable, WithProperties, WithStrictNullComparison
{
    use Exportable;

    private $headers;

    public function __construct()
    {
        $this->headers = [
            'Last-Modified' => Carbon::now()->toRfc7231String(),  
        ];
    }

    public function properties(): array
    {
        return [
            'keywords'  => 'importType=operator|headerCodeLine=2',
        ];
    }


    public function headings():array{

        return[
            'Category',
            'Product SKU',
            'Product Name',
            'Parent Article Number',
            'GTIN 13',
            'Brand',
            'Gender',
            'Size',
            'Standard Colour',
            'Product Description',
            'Front Image',
            'Back Image',
            'Side Image',
            'Detail Image',
            'Environment Image',
            'Product Material 1',
            'Material Percentage 1',
            'Certificate',
            'pre-owned',
            'Offer SKU',
            'Product ID',
            'Product ID Type',
            'Offer Description',
            'Offer Internal Description',
            'Offer Price',
            'Offer Price Additional Info',
            'Offer Quantity',
            'Minimum Quantity Alert',
            'Offer State',
            'Availability Start Date',
            'Availability End Date',
            'Logistic Class',
            'Favorite Rank',
            'Discount Price',
            'Discount Start Date',
            'Discount End Date',
            'Lead Time to Ship (in days)',
            'Update/Delete'

        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $item = collect();

        $products = SaveProduct::all();

        $count = 0;

        foreach($products as $product) {

            if($product->sku == "" ) {
                continue;
            }

            $count++;

            if($count == 1) 
            {
                $subheading = [];

                array_push($subheading, "category");
                array_push($subheading, "sku_vpn");
                array_push($subheading, 'product_name');
    
                array_push($subheading, "parent_article_number");
                array_push($subheading, "product_id1");
    
                array_push($subheading, "brand");

                array_push($subheading, "gender");
                array_push($subheading, "size");
                array_push($subheading, "standard_colour");

                array_push($subheading, "product_description");
                array_push($subheading, "image1");
                array_push($subheading, "image2");
                array_push($subheading, "image3");
                array_push($subheading, "image4");
                array_push($subheading, "image5");
                array_push($subheading, "material1");
                array_push($subheading, "material_percentage1");
                array_push($subheading, "certificate");
                array_push($subheading, "pre-owned");
                array_push($subheading, "sku");
                array_push($subheading, "product-id");
                array_push($subheading, "product-id-type");
                array_push($subheading, "description");
                array_push($subheading, "internal-description");
                array_push($subheading, "price");
                array_push($subheading, "price-additional-info");
                array_push($subheading, "quantity");
                array_push($subheading, "min-quantity-alert");
                array_push($subheading, "state");
                array_push($subheading, "available-start-date");
                array_push($subheading, "available-end-date");
                array_push($subheading, "logistic-class");
                array_push($subheading, "favorite-rank");
                array_push($subheading, "discount-price");
                array_push($subheading, "discount-start-date");
                array_push($subheading, "discount-end-date");
                array_push($subheading, "leadtime-to-ship");
                array_push($subheading, "update-delete");


                $item->push($subheading);

                
            }

            $productsCollection = [];

            if (strpos($product->product_name, 'Watch') !== false) {
                array_push($productsCollection, "Pre-owned/Vintage/Accessories"); // Category
            } else {
                array_push($productsCollection, "Pre-owned/Vintage/Bags"); // Category
            }

            array_push($productsCollection, $product->sku);
            array_push($productsCollection, $product->product_name);

            array_push($productsCollection, "");
            array_push($productsCollection, $product->sku);
            array_push($productsCollection, "A Retro Tale");
            array_push($productsCollection, "Female"); //Gender
            array_push($productsCollection, "Not Applicable");
            array_push($productsCollection,  $product->stock_status == "instock" ? $product->product_color_name : "Black");

            $general_desc = DescriptionSettings::where('market_place', 'Ahlens')->first();

            if($general_desc->status == 1) {
                $newdesc = $product->product_swedish_description . PHP_EOL . PHP_EOL .  strip_tags($general_desc->site_intro) . PHP_EOL .PHP_EOL . strip_tags($general_desc->prompt_text);
            } else {
                $newdesc = $product->product_swedish_description;
            }

            array_push($productsCollection, $newdesc );


            $a =  explode(",", $product->product_image_url);

            for ($i=0; $i < 5; $i++) { 
                if(isset($a[$i])) {
                    array_push($productsCollection, $a[$i]);
                } else {
                    array_push($productsCollection, "");

                }
                
            }

            array_push($productsCollection, "Not Applicable");
            array_push($productsCollection, "Not Applicable");
            array_push($productsCollection, "");
            array_push($productsCollection, "Vintage");
            array_push($productsCollection, $product->sku);
            array_push($productsCollection, $product->sku);
            array_push($productsCollection, "EAN");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            $getPrice = PriceAdjustment::where('market_place', 'Ahlens')->first();

            if($getPrice->status == 1) {
                $percent = floatval($product->selling_price_eur)  *  floatval($getPrice->percentage) / 100;
                $price = floatval($product->selling_price_eur) + $percent;
            } else {
                $price = floatval($product->selling_price_eur);
            }
            array_push($productsCollection, round($price) );
            array_push($productsCollection, "");
            array_push($productsCollection, $product->stock_status == "instock" ? $product->stock : 0);
            array_push($productsCollection, "");
            array_push($productsCollection, "Vintage");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, $product->stock_status == "instock" ? "Update" : "Delete");


            $item->push($productsCollection);


        }

        return $item;
    }
}
