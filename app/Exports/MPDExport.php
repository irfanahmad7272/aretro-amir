<?php

namespace App\Exports;

use App\Models\Setting;
use App\Models\SaveProduct;
use App\Models\PriceAdjustment;
use App\Models\DescriptionSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class MPDExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    public function headings(): array
    {
        return [
            'mpd_id',
            'code',
            'category_id',
            'brand_id',
            'status',
            'has_retail_tags',
            'condition_rating',
            'promotions',
            'price_chf',
            'retail_chf',
            'name_en',
            'name_fr',
            'name_de',
            'description_en',
            'description_fr',
            'description_de',
            'wear_signs_desc_en',
            'wear_signs_desc_fr',
            'wear_signs_desc_de',
            'image_url_1',
            'image_url_2',
            'image_url_3',
            'image_url_4',
            'image_url_5',
            'image_url_6',
            'image_url_7',
            'image_url_8',
            'image_url_9',
            'image_url_10'
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $item = collect();

        $products = SaveProduct::where('stock', '>', 0)
            // ->where(function ($q) {
            //     $q->whereNull('hide_at_mpd')
            //         ->orWhere('hide_at_mpd', '!=', 1);
            // })
            ->get();

        $getCHF = Setting::first();
        $chf = $getCHF->currency_swiss_franc;

        foreach ($products as $key => $product) {

            // $rebelle_cat_brand_model =  explode('>', $product->rebelle_sub_category_brand_model);
            // $cat_id = isset($rebelle_cat_brand_model[0]) ? trim($rebelle_cat_brand_model[0]) : "";

            // $brand_id = isset($rebelle_cat_brand_model[1]) ? trim($rebelle_cat_brand_model[1]) : "";

            $productsCollection = [];

            array_push($productsCollection, ""); //mpd_id
            array_push($productsCollection, $product->original_sku);  //code
            array_push($productsCollection, $product->mpd_cat_id);  //cat_id
            array_push($productsCollection, $product->mpd_brand_id);  //brand_id
            array_push($productsCollection, 'live');
            array_push($productsCollection, 0); //has_retail_tags
            array_push($productsCollection, $product->vc_product_condition == 'Very good condition' ? 'AB' : 'B'); //condition_rating
            array_push($productsCollection, 'disabled'); //promotions

            $getPrice = PriceAdjustment::where('market_place', 'Mpd')->first();

            $priceinCHF = $chf * $product->selling_price_eur;

            if ($getPrice->status == 1) {
                $percent = $priceinCHF *  $getPrice->percentage / 100;
                $price = $priceinCHF + $percent;
            } else {
                $price = $priceinCHF;
            }
            array_push($productsCollection, round($price)); //price_chf in chf
            array_push($productsCollection, ""); //retail_chf in chf
            array_push($productsCollection, $product->product_name); //name_en
            array_push($productsCollection, ""); //name_fr
            array_push($productsCollection, ""); //name_de

            $general_desc = DescriptionSettings::where('market_place', 'General')->first();

            if ($general_desc->status == 1) {
                $newdesc = strip_tags($product->description) . PHP_EOL . PHP_EOL . strip_tags($general_desc->site_intro);
            } else {
                $newdesc = strip_tags($product->description);
            }

            array_push($productsCollection, $newdesc); // description_en
            array_push($productsCollection, ""); // description_fr
            array_push($productsCollection, ""); // description_de
            array_push($productsCollection, ""); // wear_signs_desc_en
            array_push($productsCollection, ""); // wear_signs_desc_fr
            array_push($productsCollection, ""); // wear_signs_desc_de

            $a =  explode(",", $product->product_image_url);

            for ($i = 0; $i < 5; $i++) {
                if (isset($a[$i])) {
                    array_push(
                        $productsCollection,
                        $a[$i]
                    );
                } else {
                    array_push($productsCollection, "");
                }
            }

            $item->push($productsCollection);
        }

        return $item;
    }
}
