<?php

namespace App\Exports;

use App\Models\Setting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class StarExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $data;
    public $request;

    public function headings(): array
    {
        return [
            'category',
            'sku',
            'name',
            'original_total_price (JPY)',
            'custom_fees (SEK)',
            'final_purchase_price (SEK)',
        ];
    }
    public function __construct($data, $request)
    {
        $this->data = $data;
        $this->request = $request;
    }
    public function collection()
    {

        $CSVEnteries = collect();
        $request = $this->request;
        $this->data->slice(1)->each(function ($value) use ($CSVEnteries, $request) {
            $tmpCollection = collect($value)->only([1, 3, 4, 11])->values();
            $tmpCollection->splice(3, 0, [$tmpCollection[3], $request['custom_fees_percent']]);
                $star = $this->getStarPrice($tmpCollection[5], $request['currency_rate'], $request['custom_fees_percent']);
                $tmpCollection->put(4, $star['custom_fees']);
                $tmpCollection->put(5, $star['total_price']);

            $CSVEnteries->push($tmpCollection);
        });

        return $CSVEnteries;
    }


    public function getStarPrice($total_product_price, $conversion_rate, $custom_fees_rate)
    {
        $productPrice = ($total_product_price * $conversion_rate);
        $custom_fees = (floatval($custom_fees_rate) / 100) * $productPrice;
        $purchasePrice = round($productPrice + $custom_fees, 2);

        return [
            'total_price' => round($purchasePrice, 2),
            'custom_fees' => round($custom_fees, 2)
        ];
    }
}
