<?php

namespace App\Exports;

use App\Models\Setting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class AUCExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $data;
    public $request;

    public function headings(): array
    {
        return [
            'sku',
            'brand',
            'name',
            'genre',
            'line/type',
            'shape',
            'original_total_price (JPY)',
            'shipping (JPY)',
            'commission ' . $this->request['commission'] . '% (JPY)',
            'custom_fees (JPY)',
            'final_purchase_price (SEK)',
            'grade evaluation',
            'exterior',
            'interior',
        ];
    }
    public function __construct($data, $request)
    {
        $this->data = $data;
        $this->request = $request;
    }
    public function collection()
    {

        $CSVEnteries = collect();
        $request = $this->request;



        $this->data->slice(1)->each(function ($value) use ($CSVEnteries, $request) {
            $tmpCollection = collect($value)->only([2, 4, 5, 6, 7, 8, 9, 11, 12, 13])->values();

            $auc =  $this->getAucPrice($tmpCollection[6], $request);

            $tmpCollection->splice(6, 0, [$tmpCollection[6], floatval($request['shipping']), $auc['commission'], $auc['custom_fees']]);

            $tmpCollection->put(10, $auc['total_price']);

            $CSVEnteries->push($tmpCollection);
        });

        return $CSVEnteries;
    }


    public function getAucPrice($total_product_price, $request)
    {
        $productCommission =  (floatval($request['commission']) / 100) * $total_product_price;

        $productPriceWithCommission = $productCommission + $total_product_price;

        $productPriceWithShipping = ($productPriceWithCommission + floatval($request['shipping']));

        $custom_fees = (floatval($request['custom_fees_percent']) / 100) * $productPriceWithShipping;

        $productWithCustomFees = $productPriceWithShipping + $custom_fees;


        $purchasePrice = $productWithCustomFees * $request['currency_rate'];


        return [
            'total_price' => round($purchasePrice, 2),
            'commission' =>  $productCommission,
            'custom_fees' => $custom_fees
        ];
    }
}
