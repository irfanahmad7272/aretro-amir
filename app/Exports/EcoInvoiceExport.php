<?php

namespace App\Exports;

use App\Models\Setting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class EcoInvoiceExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $data;
    public $request;

    public function headings(): array
    {
        return [
            'sku',
            'name',
            'original_total_price (JPY)',
            'shipping (SEK)',
            'custom_fees (SEK)',
            'final_purchase_price (SEK)',
            'image_1',
            'image_2',
            'image_3',
            'image_4',
            'image_5',
            'image06',
            'image07',
            'image_8',
            'image_9',
            'image_10',
        ];
    }
    public function __construct($data, $request)
    {
        $this->data = $data;
        $this->request = $request;
    }
    public function collection()
    {

        $CSVEnteries = collect();
        $request = $this->request;

        $this->data->slice(2)->each(function ($value) use ($CSVEnteries, $request) {
            $tmpCollection = collect($value)->only([0, 2, 7, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])->values();
            $tmpCollection->splice(2, 0, [$tmpCollection[2], $request['shipping'], $request['custom_fees_percent']]);

            $tmpCollection->put(1, preg_replace('/\s*\([^)]*\)\s*/', '', $tmpCollection[1]));

            $eco = $this->getEcoPrice($tmpCollection[5], $request);
            $tmpCollection->put(4,   $eco['custom_fees']);
            $tmpCollection->put(5,   $eco['total_price']);
            $CSVEnteries->push($tmpCollection);
        });
        return $CSVEnteries;
    }


    public function getEcoPrice($total_product_price, $request)
    {
        $productPriceWithShipping = ($total_product_price * $request['currency_rate']) + floatval($request['shipping']);
        $custom_fees = (floatval($request['custom_fees_percent']) / 100) * $productPriceWithShipping;
        $purchasePrice =  $productPriceWithShipping + $custom_fees;

        return [
            'total_price' => round($purchasePrice, 2),
            'custom_fees' => round($custom_fees, 2)
        ];
    }
}
