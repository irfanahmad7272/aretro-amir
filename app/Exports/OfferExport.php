<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\SaveProduct;
use App\Models\PriceAdjustment;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithProperties;


class OfferExport implements FromCollection, WithHeadings, ShouldAutoSize, Responsable, WithProperties
{
    use Exportable;

    private $headers;

    public function __construct()
    {
        $this->headers = [
            'Last-Modified' => Carbon::now()->toRfc7231String(),  
        ];
    }

    public function properties(): array
    {
        return [
            'keywords'  => 'importType=operator|headerCodeLine=2',
        ];
    }

    public function headings():array{
        return[
            'Offer SKU',
            'Product ID',
            'Product ID Type',
            'Offer Description',
            'Offer Internal Description',
            'Offer Price',
            'Offer Price Additional Info',
            'Offer Quantity',
            'Minimum Quantity Alert',
            'Offer State',
            'Availability Start Date',
            'Availability End Date',
            'Logistic Class',
            'Favorite Rank',
            'Discount Price',
            'Discount Start Date',
            'Discount End Date',
            'Lead Time to Ship',
            'Update/Delete',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $item = collect();

        $products = SaveProduct::where('brand', 'gucci')->get();

        $count = 0;

        foreach($products as $key => $product) {

            $count++;

            if($count == 1) 
            {
                $subheading = [];

                array_push($subheading, "sku");
                array_push($subheading, "product-id");
                array_push($subheading, 'product-id-type');
    
                array_push($subheading, "description");
                array_push($subheading, "internal-description");
    
                array_push($subheading, "price");
    
                array_push($subheading, "price-additional-info");
                array_push($subheading, "quantity");
                array_push($subheading, "min-quantity-alert");
    
                array_push($subheading, "state");
                array_push($subheading, "available-start-date");
                array_push($subheading, "available-end-date");
                array_push($subheading, "logistic-class");
                array_push($subheading, "favorite-rank");
                array_push($subheading, "discount-price");
                array_push($subheading, "discount-start-date");
                array_push($subheading, "discount-end-date");
                array_push($subheading, "leadtime-to-ship");
                array_push($subheading, "update-delete");

                $item->push($subheading);

                
            }


            if($count == 3) {
                break;
            }

            $productsCollection = [];

            array_push($productsCollection, $product->sku);
            array_push($productsCollection, $product->sku);
            array_push($productsCollection, 'EAN');

            array_push($productsCollection, "");
            array_push($productsCollection, "");

            $getPrice = PriceAdjustment::where('market_place', 'Ahlens')->first();

            if($getPrice->status == 1) {
                $percent = $product->selling_price_eur *  $getPrice->percentage / 100;
                $price = $product->selling_price_eur + $percent;
            } else {
                $price = $product->selling_price_eur;
            }
            array_push($productsCollection, round($price));

            array_push($productsCollection, "");
            array_push($productsCollection, $product->stock);
            array_push($productsCollection, "");

            array_push($productsCollection, "Second Hand");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");


            $item->push($productsCollection);


        }

        return $item;
    }
}
