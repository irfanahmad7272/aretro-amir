<?php

namespace App\Exports;

use Throwable;
use App\API\CurrencyController;
use App\Models\SaveProduct;
use App\Models\PriceAdjustment;
use App\Models\DescriptionSettings;
use App\Models\Setting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Codexshaper\WooCommerce\Facades\Product;


class VintageBarExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    // public function __construct($collection)
    // {
    //     $this->collection = $collection;
    // }

    public function headings():array{
        return[
            'stock',
            'product_image_url',
            'sku',
            'selling_price_eur',
            'brand',
            'brand_short',
            'product_name',
            'description',
            'main_category',
            'category',
            'category_short',
            'type_of_material',
            'name_of_material',
            'type_of_material2',
            'name_of_material2',
            'style',
            'color1',
            'shade1',
            'color2',
            'shade2',
            'rrp',
            'length',
            'width',
            'depth',
            'shoulder_drop',
            'hand_drop',
            'diameter',
            'circumference',
            'ring_size',
            'belt_size',
            'carat',
            'dust_bag',
            'box',
            'authenticity_card',
            'comments',
            'rating',
            'country_of_origin',
            'directory',
            'photos',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */


    public function collection()
    {
        $item = collect();

        // $getEuro = new CurrencyController('SEK', 'EUR');

        // $euro = $getEuro->currency();

        $getEuro = Setting::first();

        $euro = $getEuro->currency_euro;

        $products = SaveProduct::where('stock', '>', 0)
        ->where(function($q) {
            $q->whereNull('hide_at_tvb')
            ->orWhere('hide_at_tvb' , '!=', 1);
        })
        ->get();

        foreach($products as $product) {

            $productsCollection = [];
            array_push($productsCollection, $product->stock);

            $priceinEuro = $euro * $product->selling_price_eur;

            $getPrice = PriceAdjustment::where('market_place', 'VintageBar')->first();

            if($getPrice->status == 1) {
                $percent = $priceinEuro *  $getPrice->percentage / 100;
                $price = $priceinEuro + $percent;
            } else {
                $price = $priceinEuro;
            }

            $general_desc = DescriptionSettings::where('market_place', 'General')->first();

            if($general_desc->status == 1) {
                $newdesc = $product->description . $general_desc->site_intro;
            } else {
                $newdesc = $product->description;
            }

            array_push($productsCollection, $product->product_image_url);
            array_push($productsCollection, $product->sku);

            array_push($productsCollection, round($price));
            array_push($productsCollection, $product->brand);
            array_push($productsCollection, "");
            array_push($productsCollection, $product->product_name);

            array_push($productsCollection,  $product->description);
            array_push($productsCollection, "");

            array_push($productsCollection, $product->category);
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, ""); //material1
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, $product->product_color_name);
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, $product->length);
            array_push($productsCollection, $product->width);
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, $product->rating);
            array_push($productsCollection, "");
            array_push($productsCollection, "");
            array_push($productsCollection, "");

            $item->push($productsCollection);

        }

        return $item;
    }
    

}
