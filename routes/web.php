<?php

use App\Models\Setting;
use App\Models\MarketPlace;
use App\Models\SaveProduct;
use App\API\CurrencyController;
use App\Models\PriceAdjustment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Products\ProductController;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});


Route::get('/update-currency-rates', function () {

    $exchangeRates = app(ExchangeRate::class);

    $result = $exchangeRates->exchangeRate('SEK', ['EUR', 'CHF']);
    $resultJPY = $exchangeRates->exchangeRate('JPY', ['SEK']);

    Setting::truncate();

    $settings = new Setting();
    $settings->currency_euro = $result["EUR"];
    $settings->currency_swiss_franc = $result["CHF"];
    $settings->jpy_to_sek = $resultJPY["SEK"];
    $settings->save();

    return "currency updated";
})->middleware("auth");

// Route::get('/update-euro-prices', function () {

//     $products = SaveProduct::all();

//     $getEuro = Setting::first();

//     $euro = $getEuro->currency_euro;


//     $getPrice = PriceAdjustment::where('market_place', 'Vestiaire')->first();

//     foreach ($products as $key => $product) {

//         $priceinEuro = $euro * floatval($product->selling_price_eur);

//         if ($getPrice->status == 1) {
//             $percent = $priceinEuro *  floatval($getPrice->percentage) / 100;
//             $price = $priceinEuro + $percent;
//         } else {
//             $price = $priceinEuro;
//         }

//         $getProduct = SaveProduct::find($product->id);

//         $getProduct->vc_price_in_euros = round($price);

//         $getProduct->save();
//     }

//     return "Prices updated successfully!";
// })->middleware("auth");

Auth::routes(['register' => false]);

Route::get('/clear', function () {

    // echo utf8_encode("Hermès");
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});



Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::group(['namespace' => 'App\Http\Controllers\Products'], function () {

    Route::group(['middleware' => ['auth']], function () {

        Route::post('/products/import', [
            'uses' => 'ProductController@importProducts',
            'as' => 'products.import',
        ]);

        Route::get('/products', [
            'uses' => 'ProductController@index',
            'as' => 'products.index',
        ]);

        Route::delete('/products', [
            'uses' => 'ProductController@delete',
            'as' => 'products.delete',
        ]);

        Route::get('vintage-bar', [
            'uses' => 'ProductController@vintage_bar',
            'as' => 'vintage_bar',
        ]);

        Route::get('vestiaire', [
            'uses' => 'ProductController@vestiaire',
            'as' => 'vestiaire',
        ]);

        Route::get('ahlens', [
            'uses' => 'ProductController@ahlens',
            'as' => 'ahlens',
        ]);

        Route::get('rebelle', [
            'uses' => 'ProductController@rebelle',
            'as' => 'rebelle',
        ]);

        Route::get('mpd', [
            'uses' => 'ProductController@mpd',
            'as' => 'mpd',
        ]);

        Route::get('fetchproducts-test', [
            'uses' => 'ProductController@job_test',
        ]);
    });

    Route::get('vintage-bar/export', [
        'uses' => 'ProductController@vintagebar_export',
        'as' => 'vintagebar.export.download',
    ]);

    Route::get('vestiaire/export', [
        'uses' => 'ProductController@vestiaire_export',
        'as' => 'vestiaire.export.download',
    ]);

    Route::get('ahlens-products/export', [
        'uses' => 'ProductController@ahlens_export',
        'as' => 'ahlens.export.download',
    ]);

    Route::get('ahlens-offers/export', [
        'uses' => 'ProductController@ahlensoffers_export',
        'as' => 'ahlensoffers.export.download',
    ]);

    Route::get('rebelle/export', [
        'uses' => 'ProductController@rebelle_export',
        'as' => 'rebelle.export.download',
    ]);

    Route::get('mpd/export', [
        'uses' => 'ProductController@mpd_export',
        'as' => 'mpd.export.download',
    ]);
});


Route::group(['namespace' => 'App\Http\Controllers\ProcessInvoices'], function () {

    Route::group(['middleware' => ['auth']], function () {
        Route::get('process-supplier-invoices', [
            'uses' => 'ProcessInvoicesController@index',
            'as' => 'process-supplier-invoices',
        ]);

        Route::post('process-supplier-invoices', [
            'uses' => 'ProcessInvoicesController@import_supplier_invoices',
            'as' => 'import-supplier-invoices',
        ]);
    });
});

Route::group(['namespace' => 'App\Http\Controllers\Settings', 'middleware' => ['auth']], function () {

    Route::get('apikey-settings', [
        'uses' => 'APIKeyController@index',
        'as' => 'apikey.index',
    ]);

    Route::post('apikey-settings', [
        'uses' => 'APIKeyController@store',
        'as' => 'apikey.store',
    ]);

    Route::get('price-adjustments', [
        'uses' => 'PriceController@index',
        'as' => 'price.index',
    ]);

    Route::post('price-adjustments', [
        'uses' => 'PriceController@update',
        'as' => 'price.update',
    ]);

    Route::get('description-settings', [
        'uses' => 'ProductDescriptionController@index',
        'as' => 'description.index',
    ]);

    Route::post('description-settings/descriptionstatus', [
        'uses' => 'ProductDescriptionController@descriptionstatus',
        'as' => 'description.status',
    ]);

    Route::post('description-settings/general', [
        'uses' => 'ProductDescriptionController@update_general_desc',
        'as' => 'description.general.update',
    ]);

    Route::post('description-settings/ahlens', [
        'uses' => 'ProductDescriptionController@update_ahlens_desc',
        'as' => 'description.ahlens.update',
    ]);

    Route::get('price-test', [
        'uses' => 'APIKeyController@price_test',
        'as' => 'price.test',
    ]);

    Route::get('truncate-prodcts-tbl', [
        'uses' => 'APIKeyController@truncate_prodcts_tbl',
        'as' => 'price.test',
    ]);
});
