<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\API\CurrencyController;
use Illuminate\Database\Seeder;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $exchangeRates = app(ExchangeRate::class);

        $result = $exchangeRates->exchangeRate('SEK', ['EUR', 'CHF']);
        $resultJPY = $exchangeRates->exchangeRate('JPY', ['SEK']);

        Setting::truncate();

        $settings = new Setting();
        $settings->currency_euro = $result["EUR"];
        $settings->currency_swiss_franc = $result["CHF"];
        $settings->jpy_to_sek = $resultJPY["SEK"];
        $settings->save();
    }
}
