<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $newUser =  User::create([
            'name' => 'Admin',
            'email' => 'fiverrlance7272@gmail.com',
            'password' => Hash::make('freeb0rd'),
            
        ]);
    }
}
