<?php

namespace Database\Seeders;

use App\Models\RebelleModel;
use Illuminate\Database\Seeder;

class RebelleModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleModel::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-models.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleModel::create([
                    'model_name' => $data['0'],
                    'model_id' => $data['1'],
                    'brand_name' => $data['2'],
                    'brand_id' => $data['3'],
                    'subcategory_name' => $data['4'],
                    'subcategory_id' => $data['5'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
