<?php

namespace Database\Seeders;

use App\Models\RebelleMaterial;
use Illuminate\Database\Seeder;

class RebelleMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleMaterial::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-materials.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleMaterial::create([
                    'material_name' => $data['0'],
                    'material_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
