<?php

namespace Database\Seeders;

use App\Models\RebelleColor;
use Illuminate\Database\Seeder;

class RebelleColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleColor::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-colors.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleColor::create([
                    'color_name' => $data['0'],
                    'color_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
