<?php

namespace Database\Seeders;

use App\Models\MarketPlace;
use Illuminate\Database\Seeder;

class MarketPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MarketPlace::truncate();

        $marketVB = new MarketPlace();

        $marketVB->market_place = "Vintagebar";
        $marketVB->file_url = url('/vintage-bar/export');
        $marketVB->save();

        $marketVC = new MarketPlace();

        $marketVC->market_place = "Vestiaire";
        $marketVC->file_url = url('/vestiaire/export');
        $marketVC->save();

        $marketAH = new MarketPlace();

        $marketAH->market_place = "Ahlens";
        $marketAH->file_url = url('/ahlens-products/export');
        $marketAH->save();

        $marketAO = new MarketPlace();

        $marketAO->market_place = "Ahlens Products";
        $marketAO->file_url = url('/ahlens-offers/export');
        $marketAO->save();

        $marketRe = new MarketPlace();

        $marketRe->market_place = "Rebelle";
        $marketRe->file_url = url('/rebelle/export');
        $marketRe->save();

        $marketMPD = new MarketPlace();

        $marketMPD->market_place = "Mpd";
        $marketMPD->file_url = url('mpd/export');
        $marketMPD->save();
    }
}
