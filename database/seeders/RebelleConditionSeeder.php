<?php

namespace Database\Seeders;

use App\Models\RebelleCondition;
use Illuminate\Database\Seeder;

class RebelleConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleCondition::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-conditions.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleCondition::create([
                    'condition_name' => $data['0'],
                    'condition_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
