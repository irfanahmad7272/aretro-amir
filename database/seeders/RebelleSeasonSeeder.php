<?php

namespace Database\Seeders;

use App\Models\RebelleSeason;
use Illuminate\Database\Seeder;

class RebelleSeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleSeason::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-seasons.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleSeason::create([
                    'season_name' => $data['0'],
                    'season_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
