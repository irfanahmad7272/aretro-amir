<?php

namespace Database\Seeders;

use App\Models\RebelleSubcategory;
use Illuminate\Database\Seeder;

class RebelleSubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleSubcategory::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-subcategories.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleSubcategory::create([
                    'subcategory_name' => $data['0'],
                    'subcategory_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
