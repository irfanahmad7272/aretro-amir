<?php

namespace Database\Seeders;

use App\Models\DescriptionSettings;
use Illuminate\Database\Seeder;

class DescriptionSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DescriptionSettings::truncate();

        $desc = new DescriptionSettings();

        $desc->market_place = "General";
        $desc->site_intro = "<p><a href='https://aretrotale.com/'>ARETROTALE.COM</a> is an entrusted online retailer based in Stockholm, which specializes in luxury vintage and second-hand accessories from the most exclusive fashion brands.</p> <p>Passionate about craftsmanship, luxury, and sustainable fashion, we offer unique, quality checked and 100% authentic items with secure and fast international shipping.</p>";
        $desc->status = 1;
        $desc->save();

        $desc = new DescriptionSettings();

        $desc->market_place = "Ahlens";
        $desc->site_intro = "<p><a href='https://aretrotale.com/'> ARETROTALE.COM </a> är en anförtrodd nätbutik baserad i Stockholm, som specialiserat sig på lyxiga vintage- och begagnade accessoarer från de mest exklusiva modemärkena.</p> <p>Vi brinner för hantverk, lyx och hållbart mode och erbjuder unika, kvalitetskontrollerade och 100% äkta artiklar med säker och snabb internationell frakt.</p>";
        $desc->prompt_text = "<p>Observera att majoriteten av våra artiklar är vintage och därför har historier att berätta om inte annat anges. Titta därför på bilder, beskrivningar och mått som tillhandahålls eftersom de ger dig den bästa uppfattningen om historien som denna artikel har att berätta.</p>";
        $desc->status = 1;
        $desc->save();
    }
}
