<?php

namespace Database\Seeders;

use App\Models\PriceAdjustment;
use Illuminate\Database\Seeder;

class PriceAdjustmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PriceAdjustment::truncate();

        $pricevb = new PriceAdjustment();

        $pricevb->market_place = "VintageBar";
        $pricevb->percentage = 3;
        $pricevb->status = 0;
        $pricevb->save();

        $pricevc = new PriceAdjustment();

        $pricevc->market_place = "Vestiaire";
        $pricevc->percentage = 15;
        $pricevc->status = 1;
        $pricevc->save();

        $priceah = new PriceAdjustment();

        $priceah->market_place = "Ahlens";
        $priceah->percentage = 17;
        $priceah->status = 1;
        $priceah->save();


        $pricearb = new PriceAdjustment();

        $pricearb->market_place = "Rebelle";
        $pricearb->percentage = 15;
        $pricearb->status = 1;
        $pricearb->save();

        $priceMPD = new PriceAdjustment();

        $priceMPD->market_place = "Mpd";
        $priceMPD->percentage = 3;
        $priceMPD->status = 1;
        $priceMPD->save();

    }
}
