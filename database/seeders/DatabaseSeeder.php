<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // ApiKeyTableSeeder::class,
            // UsersTableSeeder::class,
            // PriceAdjustmentTableSeeder::class,
            // DescriptionSettingsSeeder::class,
            // MarketPlaceSeeder::class,
            // SettingsTableSeeder::class,
            // RebelleBrandsSeeder::class,
            // RebelleColorSeeder::class,
            // RebelleConditionSeeder::class,
            // RebelleMaterialSeeder::class,
            // RebelleModelSeeder::class,
            // RebelleSeasonSeeder::class,
            // RebelleSubcategorySeeder::class
        ]);    
    }
}
