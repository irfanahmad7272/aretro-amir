<?php

namespace Database\Seeders;

use App\Models\ApiKey;
use Illuminate\Database\Seeder;

class ApiKeyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiKey::truncate();

        $key = new ApiKey();
        $key->api_key = bcrypt('api_key_artero');
        $key->save();
    }
}
