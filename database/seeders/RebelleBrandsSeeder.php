<?php

namespace Database\Seeders;

use App\Models\RebelleBrand;
use Illuminate\Database\Seeder;

class RebelleBrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RebelleBrand::truncate();
  
        $csvFile = fopen(base_path("public/rebelle-files/rebelle-brands.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                RebelleBrand::create([
                    'brand_name' => $data['0'],
                    'brand_id' => $data['1'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
    
}
