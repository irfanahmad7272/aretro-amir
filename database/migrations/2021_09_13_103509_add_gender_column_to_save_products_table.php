<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenderColumnToSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->string('stock_status')->after('stock')->nullable();
            $table->string('product_material')->after('style')->nullable();
            $table->string('product_band_material')->after('product_material')->nullable();
            $table->string('product_movement')->after('product_band_material')->nullable();
            $table->string('gender')->after('product_movement')->nullable();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('save_products', function (Blueprint $table) {
            //
        });
    }
}
