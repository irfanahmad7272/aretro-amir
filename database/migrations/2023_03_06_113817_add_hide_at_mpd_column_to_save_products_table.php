<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHideAtMpdColumnToSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->string("vc_price_in_euros")->nullable()->after('selling_price_eur');
            $table->boolean("hide_at_mpd")->default(0)->after('hide_at_rebelle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->dropColumn('vc_price_in_euros');
            $table->dropColumn('hide_at_mpd');
        });
    }
}
