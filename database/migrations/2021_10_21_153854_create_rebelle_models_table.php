<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRebelleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rebelle_models', function (Blueprint $table) {
            $table->id();
            $table->string('model_name')->nullable();
            $table->string('model_id')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('brand_id')->nullable();
            $table->string('subcategory_name')->nullable();
            $table->string('subcategory_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rebelle_models');
    }
}
