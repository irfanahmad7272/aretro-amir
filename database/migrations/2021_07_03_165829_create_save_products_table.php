<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('save_products', function (Blueprint $table) {
            $table->id();
            $table->string('product_id')->nullable();
            $table->string('stock')->nullable();
            $table->longText('product_image_url')->nullable();
            $table->string('sku')->nullable();
            $table->string('selling_price_eur')->nullable();
            $table->string('brand')->nullable();
            $table->string('brand_short')->nullable();
            $table->string('product_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('product_swedish_description')->nullable();
            $table->string('product_color_name')->nullable();
            $table->string('product_swedish_brand')->nullable();
            $table->string('main_category')->nullable();
            $table->string('category')->nullable();
            $table->string('category_short')->nullable();
            $table->string('type_of_material')->nullable();
            $table->string('name_of_material')->nullable();
            $table->string('type_of_material2')->nullable();
            $table->string('name_of_material2')->nullable();
            $table->string('style')->nullable();
            $table->string('color1')->nullable();
            $table->string('shade1')->nullable();
            $table->string('color2')->nullable();
            $table->string('shade2')->nullable();
            $table->string('rrp')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('depth')->nullable();
            $table->string('shoulder_drop')->nullable();
            $table->string('hand_drop')->nullable();
            $table->string('diameter')->nullable();
            $table->string('circumference')->nullable();
            $table->string('ring_side')->nullable();
            $table->string('belt_size')->nullable();
            $table->string('caret')->nullable();
            $table->string('dust_bag')->nullable();
            $table->string('box')->nullable();
            $table->string('authenticity_card')->nullable();
            $table->string('comments')->nullable();
            $table->string('rating')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->string('directory')->nullable();
            $table->string('photos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_products');
    }
}
