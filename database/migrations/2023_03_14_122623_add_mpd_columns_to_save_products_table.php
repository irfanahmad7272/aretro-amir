<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMpdColumnsToSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->string("mpd_brand_id")->nullable()->after("vc_product_vintage");
            $table->string("mpd_cat_id")->nullable()->after("mpd_brand_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->dropColumn("mpd_brand_id");
            $table->dropColumn("mpd_cat_id");
        });
    }
}
