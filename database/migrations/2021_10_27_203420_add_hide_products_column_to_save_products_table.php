<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHideProductsColumnToSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->boolean('hide_at_vc')->nullable();
            $table->boolean('hide_at_tvb')->nullable();
            $table->boolean('hide_at_rebelle')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('save_products', function (Blueprint $table) {
            //
        });
    }
}
