<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVcCategoriesColumnToSaveProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('save_products', function (Blueprint $table) {
            $table->string('vc_category')->after('category_short')->nullable();
            $table->string('vc_subcategory')->after('vc_category')->nullable();
            $table->string('vc_subsubcategory')->after('vc_subcategory')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('save_products', function (Blueprint $table) {
            //
        });
    }
}
